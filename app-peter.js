const fs = require("fs");
const bcrypt = require("bcrypt");

function doRead(fileName) {
  return new Promise(function(resolve, reject) {
    fs.readFile(fileName, "utf8", function(err, data) {
      if (err) {
        return reject(err);
      }
      return resolve(data);
    });
  });
}

function doWrite(data, fileName) {
  return new Promise(function(resolve, reject) {
    fs.writeFile(fileName, data, "utf8", function(err) {
      if (err) {
        return reject(err);
      }
      return resolve();
    });
  });
}

async function doHash(user) {
  const password = user.password;
  const saltRounds = 10;

  const hashedPassword = await new Promise((resolve, reject) => {
    bcrypt.hash(password, saltRounds, function(err, hash) {
      if (err) {
        return reject(err);
      }
      return resolve(hash);
    });
  });
  console.log(hashedPassword);
  return hashedPassword;
}

async function main() {
  try {
    const usersString = await doRead("./users-pw-plain.json");
    const users = JSON.parse(usersString);
    const usersHashed = await Promise.all(
      users.map(async user => ({
        ...user,
        password: await doHash(user)
      }))
    );

    await doWrite(JSON.stringify(usersHashed), "./users-pw-hashed.json");
  } catch (err) {
    console.error(err);
  }
}

main();
