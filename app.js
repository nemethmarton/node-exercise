const fs = require('fs')
const bcrypt = require('bcrypt')

function doRead() {
    return new Promise(function (resolve, reject) {
        fs.readFile('./users-pw-plain.json', 'utf8', function(err, data) {
            if (err) {
                console.log('Read error: ' + err)
            }
            resolve(JSON.parse(data))
        })
    })
}

function doWrite(data) {
    return new Promise(function (resolve, reject) {
        fs.writeFile('./users-pw-hashed.json', JSON.stringify(data), 'utf8', function(err, data) {
            if (err) {
                console.log('Write error: ' + err)
            }
            console.log('Successfully wrote file')
        })
    })
}

async function doHash (user) {

    const password = user.password
    const saltRounds = 10;
  
    const hashedPassword = await new Promise((resolve, reject) => {
      bcrypt.hash(password, saltRounds, function(err, hash) {
        if (err) reject(err)
        resolve(hash)
      });
    })
    console.log(hashedPassword)
    return hashedPassword
}

async function main() {
    var users = {}
    users = await doRead()
    for (const user of users) {
        user.password = await doHash(user)
    }
    await doWrite(users)
}

main()